# DOT in Markdown

This hack demonstrates embedding a
[DOT](http://en.wikipedia.org/wiki/DOT_language) graph in
[pandoc](http://johnmacfarlane.net/pandoc/)-flavored
[Markdown](http://daringfireball.net/projects/markdown/).

The code and build script are probably portable, but I currently only provide
instructions for Ubuntu.

Pandoc output format must be HTML or HTML5.

# How to build and view

## Prerequisites

* [pandoc](http://johnmacfarlane.net/pandoc/)
* [pandocfilters](https://pypi.python.org/pypi/pandocfilters)
* [graphviz](http://www.graphviz.org/)
* [Make](http://en.wikipedia.org/wiki/Make_(software))

## Ubuntu instructions

    sudo apt-get -y install pandoc pip graphviz make
    sudo pip install pandocfilters
    make
    gnome-open demo.html

# Screenshot

When you open `demo.html` in a web browser, it should look
[something like this](demo.png).

Saving as a PDF from the browser (via the Print function)
[should look decent too](demo.pdf).

# Copyright, License

Copyright (C)2014 Adam Monsen.

License: AGPL v3. See COPYING for details.
