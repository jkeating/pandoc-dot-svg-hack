demo.html:

%.html: %.md graphviz.py
	pandoc --filter ./graphviz.py --table-of-contents --standalone --number-sections --smart --to html5 $< > $@

clean:
	$(RM) *.html

.DELETE_ON_ERROR:
